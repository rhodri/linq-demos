﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SelectProjection
{
    public class EmployeeBasicInfo
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Salary { get; set; }
    }
}