﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SelectProjection
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("=== Basic Select ===");
            // Using Query Syntax
            basicQuery();

            // Using Method Syntax
            basicMethod();

            // Select One
            Console.WriteLine("=== Select One Property ===");
            selectOnePropertyQuery();
            selectOnePropertyMethod();

            //Select Multiple Properties
            Console.WriteLine("=== Select Multiple Properties ===");
            selectMultiplePropertiesQuery();
            selectMultiplePropertiesMethod();

            //Project to a different object
            Console.WriteLine("=== Project to a Different Object ===");
            projectToDifferentTypeMethod();
            projectToDifferentTypeQuery();

            //Project to Anonymous Method
            Console.WriteLine("=== Project to an Anonymous Type ===");
            projectToAnonymousTypeQuery();
            projectToAnonymousTypeMethod();
        }

        public static void basicQuery()
        {
            Console.WriteLine($"*** Using Query Syntax ***");
            List<Employee> basicQuery = (from emp in Employee.GetEmployees()
                                         select emp).ToList();

            foreach (Employee emp in basicQuery)
            {
                Console.WriteLine($"ID: {emp.ID} Name: {emp.FirstName} {emp.LastName}");
            }

            Console.WriteLine();
        }

        public static void basicMethod()
        {
            Console.WriteLine($"*** Using Method Syntax ***");

            IEnumerable<Employee> basicMethod = Employee.GetEmployees().ToList();

            foreach (Employee emp in basicMethod)
            {
                Console.WriteLine($"ID: {emp.ID} Name: {emp.FirstName} {emp.LastName}");
            }

            Console.WriteLine();
        }

        private static void projectToAnonymousTypeMethod()
        {
            Console.WriteLine($"*** Using Method Syntax ***");

            var selectMethod = Employee
                .GetEmployees()
                .Select(emp => new
                {
                    FirstName = emp.FirstName,
                    LastName = emp.LastName,
                    Salary = emp.Salary
                })
                .ToList();

            foreach (var emp in selectMethod)
            {
                Console.WriteLine($"Name: {emp.FirstName} {emp.LastName} Salary: {emp.Salary}");
            }
        }

        private static void projectToAnonymousTypeQuery()
        {
            Console.WriteLine($"*** Using Query Syntax ***");

            var selectQuery = (from emp in Employee.GetEmployees()
                               select new
                               {
                                   FirstName = emp.FirstName,
                                   LastName = emp.LastName,
                                   Salary = emp.Salary
                               });

            foreach (var emp in selectQuery)
            {
                Console.WriteLine($"Name: {emp.FirstName} {emp.LastName} Salary: {emp.Salary}");
            }
        }

        private static void projectToDifferentTypeQuery()
        {
            Console.WriteLine($"*** Using Query Syntax ***");

            IEnumerable<EmployeeBasicInfo> selectQuery = (from emp in Employee.GetEmployees()
                                                          select new EmployeeBasicInfo()
                                                          {
                                                              FirstName = emp.FirstName,
                                                              LastName = emp.LastName,
                                                              Salary = emp.Salary
                                                          });

            foreach (var employee in selectQuery)
            {
                Console.WriteLine($"Name: {employee.FirstName} {employee.LastName} Salary: {employee.Salary}");
            }

            Console.WriteLine();
        }

        private static void projectToDifferentTypeMethod()
        {
            Console.WriteLine($"*** Using Method Syntax ***");

            List<EmployeeBasicInfo> selectMethod = Employee
                .GetEmployees()
                .Select(emp => new EmployeeBasicInfo()
                {
                    FirstName = emp.FirstName,
                    LastName = emp.LastName,
                    Salary = emp.Salary
                })
                .ToList();

            foreach (var employee in selectMethod)
            {
                Console.WriteLine($"Name: {employee.FirstName} {employee.LastName} Salary: {employee.Salary}");
            }

            Console.WriteLine();
        }

        private static void selectOnePropertyQuery()
        {
            Console.WriteLine($"*** Using Query Syntax ***");
            List<int> basicQuery = (from emp in Employee.GetEmployees()
                                    select emp.ID).ToList();

            foreach (var id in basicQuery)
            {
                Console.WriteLine($"ID: {id}");
            }

            Console.WriteLine();
        }

        private static void selectOnePropertyMethod()
        {
            Console.WriteLine($"*** Using Method Syntax ***");

            IEnumerable<int> basicMethod = Employee.GetEmployees().Select(emp => emp.ID).ToList();

            foreach (var id in basicMethod)
            {
                Console.WriteLine($"ID: {id}");
            }

            Console.WriteLine();
        }

        private static void selectMultiplePropertiesQuery()
        {
            Console.WriteLine($"*** Using Query Syntax ***");

            IEnumerable<Employee> selectQuery =
                (from emp in Employee.GetEmployees()
                 select new Employee()
                 {
                     FirstName = emp.FirstName,
                     LastName = emp.LastName,
                     Salary = emp.Salary
                 });

            foreach (var emp in selectQuery)
            {
                Console.WriteLine($"Name: {emp.FirstName} {emp.LastName} Salary: {emp.Salary}");
            }

            Console.WriteLine();
        }

        private static void selectMultiplePropertiesMethod()
        {
            Console.WriteLine($"*** Using Method Syntax ***");

            List<Employee> selectMethod = Employee.GetEmployees().Select(emp => new Employee()
            {
                FirstName = emp.FirstName,
                LastName = emp.LastName,
                Salary = emp.Salary
            }).ToList();

            foreach (var emp in selectMethod)
            {
                Console.WriteLine($"Name: {emp.FirstName} {emp.LastName} Salary: {emp.Salary}");
            }

            Console.WriteLine();
        }
    }
}