﻿using System.Collections.Generic;

namespace SelectProjection
{
    public class Employee
    {
        public int ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Salary { get; set; }

        public static List<Employee> GetEmployees()
        {
            List<Employee> employees = new List<Employee>
            {
                new Employee {ID = 101, FirstName = "Preety", LastName = "Tiwary", Salary = 60_000 },
                new Employee {ID = 102, FirstName = "Priyanka", LastName = "Dewagan", Salary = 70_000 },
                new Employee {ID = 103, FirstName = "Hina", LastName = "Sharma", Salary = 80_000 },
                new Employee {ID = 104, FirstName = "Anurag", LastName = "Mohanty", Salary = 90_000 },
                new Employee {ID = 105, FirstName = "Sambit", LastName = "Satapathy", Salary = 100_000 },
                new Employee {ID = 106, FirstName = "Sushanta", LastName = "Jena", Salary = 160_000 },
            };

            return employees;
        }
    }
}